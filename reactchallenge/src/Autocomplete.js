import React, {useState} from 'react';
import _get from 'lodash/get';
import AsyncSelect from 'react-select/async';
import CityInfo from './CityInfo';

const Autocomplete = ({setPopulation}) => {
    const [data, setData] = useState([]);
    const [cityAbout, setCityAbout] = useState([]);
             
    const fetchData = async(keyword) => {
        const url=`https://api.teleport.org/api/cities/?search=${keyword}`;
       return await fetch(url)
        .then(response => response.json())
        .then(responseData => {
           setData(_get(responseData, '_embedded.city:search-results', []));
          return data.map(result => ({ value: result._links, label: result.matching_full_name }));
     });
    }
      
    const handleChange = async (selectedItem) =>{
        const url = _get(selectedItem,'value.city:item.href',[]);
       await fetch(url)
       .then(response => response.json())
       .then(response => setCityAbout({full_name: response.full_name,
                geoname_id: response.geoname_id,
                population: response.population}));      
    }

    return (
        <div>
            <AsyncSelect className="select" 
            loadOptions={fetchData} 
            placeholder="search :)" 
            onChange={handleChange}
            isClearable="true" 
            />   
            <CityInfo 
                full_name={cityAbout.full_name}
                geoname_id={cityAbout.geoname_id}
                population={cityAbout.population}/>         
        </div>
    )
}

export default Autocomplete;
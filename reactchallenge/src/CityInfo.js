import React from 'react';

const CityInfo = ({full_name, geoname_id, population}) => {



    return (
        <div>
            <strong>Name :</strong>
            <div>{full_name}</div>

            <strong>Geoname :</strong>
            <div>{geoname_id}</div>

            <strong>Population :</strong>
            <div>{population}</div>
        </div>
    )

}

export default CityInfo;